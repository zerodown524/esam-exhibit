This project reflects my abilities several years ago - prior to any college or professional experience coding.

I make updates and adjustments from time to time - but I have no plans to rework it entirely since it's been functional at the museum for several years.

#Instructions

You will need to make folder named "video" in the same directory as the executable, as well as the config.properties file.

Video can be obtained from public google drive link here --> https://drive.google.com/file/d/0ByWSC2Fp-1NiZlhfTVRDWkd2YTA/view?usp=sharing - just download it to the /video folder

Build the project on openjdk 1.8 with maven package command.

To run the program, copy the esam_exhibit-1.0-SNAPSHOT-jar-with-dependencies.jar from the target folder to the root of the project (same folder as /video and config.properties)

Launch the program (java -jar esam_exhibit-1.0-SNAPSHOT-jar-with-dependencies.jar)

    - Pick "GUI"

    - Tick the red box (NO GO!)

    - Hit count down when the video pauses

    - Then press launch when the video pauses again

    - Uncheck the green box (GO!) - video should reset

   You can play with settings in the configuration file (config.properties)

Develop/Production

    - Production is full screen with no pop-ups (designed to be launched on system startup autonomously)

    - Develop is windowed and prompts for how to accept input

Timings can also be adjusted

