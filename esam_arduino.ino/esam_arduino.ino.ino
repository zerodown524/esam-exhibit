const int S2_pin = 2, S3_pin=3, S4_pin=4, S5_pin=5, S6_pin=6, CD_pin = 7, L_pin = 8;

void setup() {
pinMode(S2_pin, INPUT);
pinMode(S3_pin, INPUT);
pinMode(S4_pin, INPUT);
pinMode(S5_pin, INPUT);
pinMode(S6_pin, INPUT);
pinMode(CD_pin, INPUT);
pinMode(L_pin, INPUT);
Serial.begin(9600);
}
void loop()
{

  if (digitalRead(S2_pin) == HIGH && digitalRead(S3_pin) == HIGH && digitalRead(S4_pin) == HIGH && digitalRead(S5_pin) == HIGH && digitalRead(S6_pin) == HIGH && digitalRead(CD_pin) == HIGH && digitalRead(L_pin) == HIGH) {
    Serial.println(3);
  }
      
  else if (digitalRead(S2_pin) == HIGH && digitalRead(S3_pin) == HIGH && digitalRead(S4_pin) == HIGH && digitalRead(S5_pin) == HIGH && digitalRead(S6_pin) == HIGH && digitalRead(CD_pin) == HIGH && digitalRead(L_pin) != HIGH) {
    
    Serial.println(2);
  }  
  
  else if (digitalRead(S2_pin) == HIGH && digitalRead(S3_pin) == HIGH && digitalRead(S4_pin) == HIGH && digitalRead(S5_pin) == HIGH && digitalRead(S6_pin) == HIGH && digitalRead(CD_pin) != HIGH && digitalRead(L_pin) != HIGH) {
    Serial.println(1);  }
  
  else if (digitalRead(S2_pin) != HIGH && digitalRead(S3_pin) != HIGH && digitalRead(S4_pin) != HIGH && digitalRead(S5_pin) != HIGH && digitalRead(S6_pin) != HIGH && digitalRead(CD_pin) != HIGH && digitalRead(L_pin) != HIGH) {
    Serial.println(0);  }
    
  delay(100);
}

