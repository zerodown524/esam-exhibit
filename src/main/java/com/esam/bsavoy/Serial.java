/*
 *This class sets up the serial ports and listeners
 */
package com.esam.bsavoy;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;

public class Serial implements SerialPortEventListener, Runnable {

    static Logger logger = Logger.getLogger(Serial.class);

    public static String val;

    private static SerialPort serialPort;

    private BufferedReader input;

    private OutputStream output;

    private static final int TIME_OUT = 2000;

    private static final int DATA_RATE = 9600;

    //Prompt the user if no serial device has been found.
    public static void prompt_Serial_Not_Found() {
        JFrame frame = new JFrame();
        Object[] options = {"Yes", "No"};
        logger.info("Waiting for user decision in GUI...");
        int selection = JOptionPane.showOptionDialog(frame, "Could not find the serial device! Try to plug the device into USB again...", "Check again?",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        logger.info("Selection has been made!");
        if (selection == 0) {
            //logger.info("Program: GUI input method choosen!");
            logger.info("User elected to try checking for serial device again!");
        } else if (selection == 1) {
            logger.warn("User has elected to exit the program!");
            Serial.set_CleanKill(1);
        }
    }

    //Get the current value reported by the serial device
    public static int get_Serial() {
        logger.info("Value recieved is: " + val);
        int int_Val = 0;
        try {
            int_Val = Integer.parseInt(val);
        } catch (NumberFormatException n) {
            logger.warn("Well that was embarassing....");
            logger.warn("Returning current state rather than nothing: " + Sequence.get_State());
            int_Val = Sequence.get_State();
        }
        return (int_Val);
    }

    //Cleanly kill the program, including closing the serial port
    public static void set_CleanKill(int returnCode) {
        if (serialPort != null) {
            serialPort.removeEventListener();
            logger.info("Serial port released!");
        }
        System.exit(returnCode);
    }

    //Just wait for a few milliseconds
    public static void be_Patient(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            logger.fatal("There was a problem when trying to get sequence to sleep!");
            logger.fatal("Exiting with status: 2");
            Serial.set_CleanKill(2);
        }
    }

    //Send the serial event to the program thread for handling

    public synchronized void serialEvent(SerialPortEvent oEvent) {
        if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
                if (input.ready()) {

                    //This part can be tricky... port can sometimes recieve garbage that cannot be parsed (try statement to capture throws infinite loop)
                    String val = input.readLine();

                    int val_Int = 0;
                    try {
                        val_Int = Integer.parseInt(val);
                    } catch (NumberFormatException n) {
                        logger.error("Invalid input from control unit: " + val);
                    }

                    //Send this information to the main thread which will handle the input
                    Sequence.set_MachineState(val_Int);
                }
            } catch (Exception e) {
                logger.error("Trouble reading the serial input!");
                logger.fatal("Exiting with return code of: 2");
                logger.fatal(e);
                set_CleanKill(2);
            }

        }

    }

    //Primary setup

    public synchronized void run() {
        int checkForSerialCount = 0;
        Props serialProperties = new Props();
        logger.info("Begin initialization");

        String portName = serialProperties.getPropValues("serialPortName");

        System.setProperty("gnu.io.rxtx.SerialPorts", portName);

        logger.info("Serial port has name of: " + System.getProperty("gnu.io.rxtx.SerialPorts"));
        CommPortIdentifier portId = null;

        //First, Find an instance of serial port as set in PORT_NAMES.
        logger.info("Looking for serial port...");

        while (portId == null) {
            checkForSerialCount++;
            Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();

            if (currPortId.getName().equals(portName)) {
                portId = currPortId;
                break;

            }
            //Can't find any? Check 4 times them prompt user for input
            logger.warn("Couldn't find any serial ports named " + portName);
            be_Patient(100);

            if (checkForSerialCount > 4) {
                prompt_Serial_Not_Found();
                checkForSerialCount = 0;
            }

        }
        logger.info("Serial port is: " + portName);

        try {
            // open serial port, and use class name for the appName.
            serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            // open the streams
            input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
            output = serialPort.getOutputStream();

            // add event listeners
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        } catch (Exception e) {

            logger.error("Couldn't setup serial port!");
            logger.fatal("Exiting with return code: 2");
            logger.error("Printing stack trace...");
            logger.error(e);
            set_CleanKill(2);
        }
        logger.info("Finished initialization!");
    }

}
