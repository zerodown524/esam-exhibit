/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esam.bsavoy;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Bradley
 */
public class Props {

    static Logger logger = Logger.getLogger(Props.class);

    public String getPropValues(String sel) {
        Properties prop = new Properties();
        String propFileName = "/config.properties";

        
        try {
            FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir") + propFileName);
            prop.load(fileInputStream);
            if (fileInputStream == null) {
                throw new FileNotFoundException("Property file '" + propFileName + "' not found in the classpath");
            }
            if (sel.equals("serialPortName")) {
                String result = prop.getProperty("serialPortName");
                logger.info("Configured serial port: " + result);
                return result;

            } else if (sel.equals("runMode")) {
                String result = prop.getProperty("runMode");
                logger.info("Configured run mode was: " + result);
                return result;

            }else if (sel.equals("videoName")) {
                String result = prop.getProperty("videoName");
                logger.info("Configured video name: " + result);
                return result;

            }else if (sel.equals("videoDir")) {
                String result = prop.getProperty("videoDir");
                logger.info("Configured video directory: " + result);
                return result;

            } else if (sel.equals("videoCountDownPauseTime")) {
                String result = prop.getProperty("videoCountDownPauseTime");
                logger.info("Configured count down pause time: " + result);
                return result;

            } else if (sel.equals("videoLaunchPauseTime")) {
                String result = prop.getProperty("videoLaunchPauseTime");
                logger.info("Configured launch pause time: " + result);
                return result;

            } else if (sel.equals("videoEndPauseTime")) {
                String result = prop.getProperty("videoEndPauseTime");
                logger.info("Configured video launch pause time: " + result);
                return result;

            } else if (sel.equals("videoStandbyTimer")) {
                String result = prop.getProperty("videoStandbyTimer");
                logger.info("Configured standby mode time to wait duration: " + result);
                return result;

            } else {
                logger.error("Property " + sel + " not found! Exiting!");
                Serial.set_CleanKill(2);
                return "2";
            }   
        } catch (IOException e) {
            return ("2");
        }
    }
}
