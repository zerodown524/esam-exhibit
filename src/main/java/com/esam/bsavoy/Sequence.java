/*
 * This class is responsible for watching the video process and pausing when appropriate
 */
package com.esam.bsavoy;

import javafx.util.Duration;

import org.apache.log4j.Logger;

public class Sequence implements Runnable {
    static Logger logger = Logger.getLogger(Sequence.class);
    
    private static int state = 0;
    
    //Retrieve the current status of the sequence
    public static int get_State() {
        return (state);
    }
    
    //Update the current status of the sequence
    public static void update_State(int desiredState) {
        state = desiredState;
        logger.info("Entering state: " + state);

    }

    //Wait for a couple milliseconds
    public static void be_Patient(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            logger.fatal("There was a problem when trying to get sequence to sleep!");
            logger.fatal("Exiting with status: 2");
            Serial.set_CleanKill(2);
        }
    }
    
    //Set the progress level based on the value passed into this function
    public static void set_MachineState(int value) {
        switch (value) {

            //Preflight switches are not set to GO
            case 0:
                //Reset the exhibit if it is status is 4 since this can only be achieved when the sequence has reached the end.
                if (Sequence.get_State() == 4) {
                    logger.info("Resetting exhibit...");
                    update_State(0);
                    Player.player_Reset();

                } else if (Sequence.get_State() == 0) {

                }

                break;

            case 1:

                if (Sequence.get_State() == 0 && Player.player_IsPlaying() == false) {
                    logger.info("Preflight is set to GO!");
                    Player.player_Play();
                    update_State(1);
                }

                break;

            case 2:
                if (Sequence.get_State() == 1 && Player.player_IsPlaying() == false) {
                    logger.info("Count down initiated!");
                    Player.player_Play();
                    update_State(2);
                }

                break;

            case 3:
                if (Sequence.get_State() == 2 && Player.player_IsPlaying() == false) {
                    logger.info("Launch initiated!");
                    Player.player_Play();
                    update_State(3);
                }
                
                break;

            default:
                logger.fatal("The value read from the controller was not 1-3 integer! It was: " + value);
                Serial.set_CleanKill(2);
                break;
        }
    }
    
    //Primary running sequence for exhibit

    public void run() {
        int cycle = 0;
        logger.info("Begin initialization");
        
        Props timeProperties = new Props();
        Duration countDownPauseTime = new Duration(Double.valueOf(timeProperties.getPropValues("videoCountDownPauseTime")));
        Duration launchPauseTime = new Duration(Double.valueOf(timeProperties.getPropValues("videoLaunchPauseTime")));
        Duration endPauseTime = new Duration(Double.valueOf(timeProperties.getPropValues("videoEndPauseTime")));
        
        logger.info("Finished initialization");
        
        logger.info("Starting initial cycle: " + cycle);
        
        do {
            
            logger.info("Start wait for preflight: " + state);

            //Waiting for user to set the preflight switch(s)
            do {
                be_Patient(50);
            } while (Player.get_Index().lessThanOrEqualTo(countDownPauseTime));
            Player.player_Pause();
            logger.info("Sent pause at index: " + Player.get_Index().toString());
            logger.info("End preflight sequence: " + state);

            //Waiting for target launch pause time
            do {
                be_Patient(50);
            } while (Player.get_Index().lessThanOrEqualTo(launchPauseTime));

            Player.player_Pause();
            logger.info("Paused at index: " + Player.get_Index().toString());
            logger.info("End count down sequence: " + state);

            //Waiting for target video end time and pause
            do {
                be_Patient(50);
            } while (Player.get_Index().lessThanOrEqualTo(endPauseTime));

                       
            Player.player_Pause();
            logger.info("Paused at index: " + Player.get_Index().toString());
            logger.info("End exhibit sequence: " + state);  
            
            //Waiting for reset stage
            update_State(4);
            logger.info("State updated to: " + state);
            logger.info("Entering wait for reset loop...");
            do {
                be_Patient(50);
            } while (get_State() != 0);
            cycle++;
            logger.info("Achieved cycle: " + cycle);
        } while (true);

    }
}
