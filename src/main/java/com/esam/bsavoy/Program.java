/**
 * This program was developed strictly for non-profit educational use at ESAM (Empire State Aero Science Museum) in
 * Scotia, NY, USA and was developed at no cost. 
 * This program is licensed under GPL2.0. If you are not familiar with this license, you may
 * view it here: http://www.gnu.org/licenses/gpl-2.0.html 
 * Contact: Bradley Savoy
 * Phone: 518-727-7709
 * Email: brad.savoy@gmail.com
 * 
 * This program simulates a rocket launch by pausing a 
 * video and waiting for user input from either a development
 * GUI, serial device, or benchmarking utility.
 * User input for the preflight, countdown and launch. The
 * program will then wait for the user to flip all preflight condition switches back to "NOGO" before
 * restarting the sequence and waiting for the next preflight check to complete (all switches set to "GO").
 * 
 * Version Notes =======1.00========= Intended for use on windows based computers
 * Tested on Win7x64 and Win8.1x64
 * MUST be run on JRE 1.7 update 21
 *
 * Date 11/28/2014
 */
package com.esam.bsavoy;

import javafx.application.Application;
import javafx.stage.Stage;
import javax.swing.*;
import org.apache.log4j.Logger;

public class Program extends Application {

    public static final String version = "Esam rocket launcher v1.00";
    static Logger logger = Logger.getLogger(Program.class);

    //Prompt the user for the input rather than assume input type during development mode

    public static int input_Chooser() {
        JFrame frame = new JFrame();
        Object[] options = {"GUI", "Serial"};

        logger.info("Waiting for user input in GUI...");

        int selection = JOptionPane.showOptionDialog(frame, "Would you like to use the contol GUI or use serial input?", "Choose Input Method",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

        logger.info("Selection has been made");
        if (selection == 0) {
            logger.info("GUI input method chosen!");
        } else if (selection == 1) {
            logger.info("Serial input method chosen!");
        }

        return (selection);
    }

    public static synchronized void do_StartupWithoutSerial() {
        Stage defaultStage = new Stage();
        Player player = new Player();
        synchronized (player) {
            logger.info("Initializing player...");
            player.initialize(defaultStage);

            logger.info("Initializing sequence thread...");
            Thread sequenceThread = new Thread(new Sequence());
            synchronized (sequenceThread) {
                sequenceThread.start();

            }
        }

    }

    public static synchronized void do_StartupWithSerial() {
        logger.info("Initializing serialReader thread...");
        Thread serialThreadProd = new Thread(new Serial());
        synchronized (serialThreadProd) {
            serialThreadProd.start();
            try {
                serialThreadProd.wait();
                do_StartupWithoutSerial();

                serialThreadProd.notify();
            } catch (InterruptedException e) {
                logger.fatal("Failed to wait! Exiting with return code: 3");
                logger.fatal(e);
                Serial.set_CleanKill(3);
            }
        }
    }

    public static synchronized void do_StatupWithBenchmark() {
        logger.info("Benchmarking utility selected!");
        logger.info("Hint: This is done when you close the input dialog");

        Thread benchmarkThread = new Thread(new Bench());

        synchronized (benchmarkThread) {
            do_StartupWithoutSerial();
            benchmarkThread.start();
            
        }

    }

    public static void do_StartupWithGUI() {

        do_StartupWithoutSerial();
        logger.info("Initializing GUI Control panel...");
        GUICntrl gui = new GUICntrl();
        gui.initialize();

    }

    //Main program

    public static void main(String[] args) {
        // Launch the JavaFX application
        Application.launch(args);
    }

    @Override
    public void start(Stage defaultStage) {
        Props programProperties = new Props();
        logger.info("ESAM vanguard exhibit version: " + version + " has started on platform: " + System.getProperty("os.name") + " on host " + System.getenv("COMPUTERNAME"));

        String runMode = programProperties.getPropValues("runMode");

        switch (runMode) {

            case "production":
                do_StartupWithSerial();
                break;

            case "develop":
                int selection = input_Chooser();
                //Let the user choose the input
                if (selection == 1) {
                    do_StartupWithSerial();

                } else if (selection == 0) {
                    do_StartupWithGUI();

                } else if (selection == -1) {

                    do_StatupWithBenchmark();
                }

                break;

            default:
                logger.fatal("Invalid run mode specified! Must be either production or develop !");
                logger.fatal("Exiting with return code: 2");
                System.exit(2);
                break;
        }

        /*This thread will continuously loop and through the state model that 
         the exhibit should follow.
         */
    }

}
