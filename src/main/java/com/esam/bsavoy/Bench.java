/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esam.bsavoy;
import org.apache.log4j.Logger;;

/**
 *
 * @author Bradley
 */
public class Bench implements Runnable {
    static Logger logger = Logger.getLogger(Bench.class);

    public void run() {
                Props timeProperties = new Props();
        int countDownPauseTime = Integer.valueOf(timeProperties.getPropValues("videoCountDownPauseTime"));
        int launchPauseTime =  Integer.valueOf(timeProperties.getPropValues("videoLaunchPauseTime"));
        int endPauseTime =  Integer.valueOf(timeProperties.getPropValues("videoEndPauseTime"));
        
        logger.info("Start initialization...");
        int cycle = 0;
        while (true) {
            logger.debug("Initialization finished");
            jvm_Stats();
            
            while (Sequence.get_State() == 4) {
               logger.debug("Faking input of 0");
                Sequence.set_MachineState(0);
               be_Patient(1000);
            }
            while (Sequence.get_State() == 0) {
               logger.debug("Faking input of 1");
               Sequence.set_MachineState(1);
               be_Patient(countDownPauseTime+1000);
            }
            while (Sequence.get_State() == 1) {
               logger.debug("Faking input of 2");
                Sequence.set_MachineState(2);
               be_Patient(launchPauseTime+1000);
            }
            while (Sequence.get_State() == 2) {
               logger.debug("Faking input of 3");
               Sequence.set_MachineState(3);
               be_Patient(endPauseTime+1000);
            }
         cycle++;
         logger.info("Finished benchmark cycle: " + cycle);
           
                
        }
        
    }
    
    public static void be_Patient(int sleep) {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            logger.fatal("There was a problem when trying to get sequence to sleep!");
            logger.fatal("Exiting with status: 2");
            Serial.set_CleanKill(2);
        }
    }
    
    public static void jvm_Stats() {
         
        int mb = 1024*1024;
         
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
         
        logger.info("##### Heap utilization statistics [MB] #####");
         
        //Print used memory
        logger.info("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
 
        //Print free memory
        logger.info("Free Memory:"
            + runtime.freeMemory() / mb);
         
        //Print total available memory
        logger.info("Total Memory:" + runtime.totalMemory() / mb);
 
        //Print Maximum available memory
        logger.info("Max Memory:" + runtime.maxMemory() / mb);
    }
}
