/**
 * This class is responsible for building the player and staging it
 */
package com.esam.bsavoy;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.media.MediaView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import javax.swing.*;
import org.apache.log4j.Logger;

public class Player {

    static Logger logger = Logger.getLogger(Player.class);
    public static MediaPlayer pointToPlayer;

    //Create the player
    public static Duration get_Index() {
        Duration currentTime;

        currentTime = pointToPlayer.getCurrentTime();
        return (currentTime);
    }

    public static void player_Pause() {
        pointToPlayer.pause();
        logger.info("Paused at index: " + get_Index().toString());
    }

    public static void player_Play() {
        pointToPlayer.play();
        logger.info("Playing at index: " + get_Index().toString());
    }

    public static void player_Reset() {

        Duration resetTime = new Duration(0);
        pointToPlayer.seek(resetTime);
        Sequence.update_State(0);
        logger.info("Reset initated");

    }

    public static boolean player_IsPlaying() {

        return (pointToPlayer.getStatus().equals(Status.PLAYING));
    }

    //Wait for a couple milliseconds

    public static void be_Patient(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            logger.fatal("There was a problem when trying to get sequence to sleep!");
            logger.fatal("Exiting with status: 2");
            Serial.set_CleanKill(2);
        }
    }

    public static void initialize(Stage stageA) {

        logger.info("Begin initialization...");

        MediaSel mediaSelection = new MediaSel();

        final String filename = mediaSelection.SelectMedia();
        try {
            Media media = new Media(filename);

            //This is the mediaplayer class being passed the media class
            MediaPlayer mediaPlayer = new MediaPlayer(media);
            pointToPlayer = mediaPlayer;

        } catch (MediaException e) {

            JOptionPane.showMessageDialog(null, "There was a problem finding the media file:\n " + filename
                    + "\nCheck that the file is located in this directory and named properly then re-run the program.\n\tPress OK to exit!",
                    "Could not load video file!", JOptionPane.ERROR_MESSAGE);
            logger.error("Couldn't locate the media file: " + filename);
            logger.error("Exiting with user error return code: 1");
            Serial.set_CleanKill(1);
        }
        //Create group for scene
        final Group root = new Group();

        stageA.setTitle(Program.version);

        // Create node of type MediaView (mediaview) from which to output video from mediaPlayer 
        MediaView mediaView = new MediaView(pointToPlayer);

        //Get the width and height properties and set them in their respective variables
        Screen screen = Screen.getPrimary();
         DoubleProperty width = mediaView.fitWidthProperty();
         DoubleProperty height = mediaView.fitHeightProperty();
         //Bind the width to the scene properties
         width.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
         height.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));


        //Set dimensions for group root 
        Scene scene = new Scene(root, 640, 360);

        //Bind mediaView node to children of group root
        ((Group) scene.getRoot()).getChildren().add(mediaView);

        //Set media start and stop times
        Duration startTime = new Duration(0);

        pointToPlayer.setStartTime(startTime);

        //Toggles media player autostart
        pointToPlayer.setAutoPlay(false);

        //Lets also preserve the aspect ratio
        mediaView.setPreserveRatio(false);

        //If we're in production mode, set the player to full screen
        Props playerProperties = new Props();

        if (playerProperties.getPropValues("runMode").equals("production")) {
            stageA.setFullScreen(true);
        }
        //Show the player
        stageA.setScene(scene);
        stageA.show();
                
        logger.info("Finished initialization");

        ////Finally, set the close event on the window so we have a nice clean break
        stageA.setOnCloseRequest(new EventHandler<WindowEvent>() {

            public void handle(WindowEvent event) {
                logger.warn("Application closed by user!");
                logger.info("Exiting by user request, return code: 0");
                Serial.set_CleanKill(0);
            }
        });
    }
}
