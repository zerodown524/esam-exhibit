/**
 * Class to create a simple control GUI which simulates input from the control
 * board. Will be kept for a future "setup mode" and for debugging purposes
 * during production usage.
 */
package com.esam.bsavoy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.apache.log4j.Logger;

public class GUICntrl extends JFrame {
    
    static Logger logger = Logger.getLogger(GUICntrl.class);
    //This sets the count down check box text and color to match the current state
    public class PreflightCheckbox implements ActionListener {


        public void actionPerformed(ActionEvent e) {

            if (read_Box() == true) {
                preflight_OK.setText("GO!");
                logger.info("Preflight set to GO!");
                preflight_OK.setBackground(Color.green);
                Sequence.set_MachineState(1);
                update_Status(1);
                
            } else if (read_Box() == false) {
                preflight_OK.setText("NOGO!");
                logger.info("Preflight set to NOGO!");
                preflight_OK.setBackground(Color.red);
                update_Status(0);
                Sequence.set_MachineState(0);

            }
        }
    }

    public class CountDownButtonHandler implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (read_Box() == true) {
                Sequence.set_MachineState(2);
                update_Status(2);
                logger.info("Count down button pressed");
            }
        }
    }

    public class LaunchButtonHandler implements ActionListener {


        public void actionPerformed(ActionEvent e) {
            if (read_Box() == true) {
                Sequence.set_MachineState(3);
                update_Status(3);
                logger.info("Launch button pressed");
            }
        }
    }

    public class ExitButtonHandler implements ActionListener {


        public void actionPerformed(ActionEvent e) {
            logger.info("Exiting by user request, return code: 0");
            System.exit(0);
        }

    }

    //This updates the status_Box to the current state of the video.
    public void update_Status(int status) {
        switch (status) {
            case 0:
                pgStatus_Box.setText("Set check box to 'GO!'");
                break;

            case 1:
                pgStatus_Box.setText("Preflight Go!!");
                break;

            case 2:
                pgStatus_Box.setText("Count down Go!!");
                break;
            case 3:
                pgStatus_Box.setText("Launch Go!");
                break;

            case 4:
                pgStatus_Box.setText("Set to check box to 'No Go'!");
                break;

            default:
                JOptionPane.showMessageDialog(null, "I'm not sure how you did "
                        + "it, but you passed something other than 0-4 for a "
                        + "status! IMPRESSIVE!", "Error: Exiting program",
                        JOptionPane.ERROR_MESSAGE);
                logger.error("Recieved invalid input of: " + status);
                logger.error("Exiting with status: 2");
                Serial.set_CleanKill(2);
                break;
        }

    }

    //This checks for the current state of the count down check box.
    public boolean read_Box() {
        boolean value = preflight_OK.isSelected();
        return (value);
    }

    //Instantiate updateable components
    private final JTextField timeBox = new JTextField("Current State:");
    private final JCheckBox preflight_OK = new JCheckBox("NO GO!");
    private final JTextField pgStatus_Box = new JTextField("Set preflight to go!");

    public void initialize() {
        logger.info("Begin initialization");
        //Create frame to hold our components
        GridLayout gridL = new GridLayout(3, 0);
        JFrame controlPanel = new JFrame("Control Panel");
        controlPanel.setSize(400, 200);
        controlPanel.setLocation(100, 100);
        controlPanel.setLayout(gridL);

        //Create components and set characteristics
        JButton countdown = new JButton("Countdown");
        JButton launch = new JButton("LAUNCH!");
        JButton exit = new JButton("Exit");

        //Initial checkbox settings
        preflight_OK.setBackground(Color.red);
        preflight_OK.setSelected(false);

        //Add components to window
        controlPanel.add(countdown);
        controlPanel.add(exit);
        controlPanel.add(launch);
        controlPanel.add(preflight_OK);
        controlPanel.add(timeBox);
        controlPanel.add(pgStatus_Box);

        //Instantiate event handlers
        CountDownButtonHandler playHandler = new CountDownButtonHandler();
        ExitButtonHandler exitHandler = new ExitButtonHandler();
        PreflightCheckbox cdIndicator = new PreflightCheckbox();
        LaunchButtonHandler launchHandler = new LaunchButtonHandler();

        //Register event handlers
        countdown.addActionListener(playHandler);
        exit.addActionListener(exitHandler);
        launch.addActionListener(launchHandler);
        preflight_OK.addActionListener(cdIndicator);

        //Finally make visible
        controlPanel.setVisible(true);
        controlPanel.setAlwaysOnTop(true);
        logger.info("Initialized");
    }
}
