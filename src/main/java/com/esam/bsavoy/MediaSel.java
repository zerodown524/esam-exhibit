/**
 * Select media used to pick media for playing. Try to validate whenever
 * possible and return "human" answers/instructions
 *
 */
package com.esam.bsavoy;

import java.io.File;
import java.net.URI;
import org.apache.log4j.Logger;


public class MediaSel {
    static Logger logger = Logger.getLogger(MediaSel.class);
    public String SelectMedia() {
        Props mediaInfo = new Props();

        String file = mediaInfo.getPropValues("videoName");
        
        logger.info("Operating system is: " + System.getProperty("os.name"));
        
        String mediaDir = mediaInfo.getPropValues("videoDir");
        if(mediaDir == null ) {
            mediaDir=System.getProperty("user.dir") + "/video/";
        } 
        File mediaFile = new File(mediaDir + file);

        //Change path to URI
        URI mediaURI = mediaFile.toURI();

        //Change back to string for media player
        String fileName = mediaURI.toString();
        //Return file name in URL encoding
        logger.info("Media selected was " + mediaFile);
        return (fileName);
    }

}
